#!/bin/sh

curl -X PUT "localhost:9200/cnx_flows" -H 'Content-Type: application/json' -d'
{
  "mappings": {
	"_doc": {
  	"properties": {
		"start_time": {
			"type": "date",
			"format": "epoch_second"
		},
		"end_time": {
			"type": "date",
			"format": "epoch_second"
		},
		"policies": {
			"type": "nested",
			"properties": {
				"all_policies": {"type": "keyword"}
			}
		},
		"source_labels": {
			"type": "nested",
			"properties": {
				"labels": {"type": "keyword"}
			}
		},
		"dest_labels": {
			"type": "nested",
			"properties": {
				"labels": {"type": "keyword"}
			}
		},
		"action": {
			"type": "keyword"
		},
		"bytes_in": {
			"type": "long"
		},
		"bytes_out": {
			"type": "long"
		},
		"dest_ip": {
			/* Will be populated with a 0.0.0.0 when no value is provided. */
			"type": "ip",
			"null_value": "0.0.0.0"
		},
		"dest_name": {
			"type": "keyword"
		},
		"dest_namespace": {
			"type": "keyword"
		},
		"dest_port": {
			/* Will be populated with a 0 when no value is provided. */
			"type": "long",
			"null_value": "0"
		},
		"dest_type": {
			"type": "keyword"
		},
		"reporter": {
			"type": "keyword"
		},
		"num_flows": {
			"type": "long"
		},
		"num_flows_completed": {
			"type": "long"
		},
		"num_flows_started": {
			"type": "long"
		},
		"packets_in": {
			"type": "long"
		},
		"packets_out": {
			"type": "long"
		},
		"proto": {
			"type": "keyword"
		},
		"source_ip": {
			/* Will be populated with a 0.0.0.0 when no value is provided. */
			"type": "ip",
			"null_value": "0.0.0.0"
		},
		"source_name": {
			"type": "keyword"
		},
		"source_namespace": {
			"type": "keyword"
		},
		"source_port": {
			/* Will be populated with a 0 when no value is provided. */
			"type": "long",
			"null_value": "0"
		},
		"source_type": {
			"type": "keyword"

		}
	}
	}
  }
}
'
