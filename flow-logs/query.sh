#!/bin/sh

curl -XGET "http://localhost:9200/cnx_flows/_search" -H 'Content-Type: application/json' -d'
{
  "size": 0,
  "query": {
    "bool": {
      "must": [
	/* Customize the "gte" value to select a time range of data */
        {"range": {"end_time": { "gte": "now-24h", "lte": "now"}}},
	/* Modify this to filter by source_type. To query all source types, remove the next line */
        {"terms": {"source_type": ["wep","network"]}},
	/* Modify this to filter by dest_type. To query all dest types, remove the next line */
        {"terms": {"dest_type": ["wep"]}}
        ]
      }
  },
  "aggs": {
    "flog_buckets": {
      "composite": {
        /* For now we set this to 10000 to return all data, since the composite query returns only 10 by default */
        "size": 10000,
        "sources": [
          {
            "source_type": {
              "terms": {
                "field": "source_type"
              }
            }
          },
          {
            "source_namespace": {
              "terms": {
                "field": "source_namespace"
              }
            }
          },
          {
            "source_name": {
              "terms": {
                "field": "source_name"
              }
            }
          },
          {
            "dest_type": {
              "terms": {
                "field": "dest_type"
              }
            }
          },
          {
            "dest_namespace": {
              "terms": {
                "field": "dest_namespace"
              }
            }
          },
          {
            "dest_name": {
              "terms": {
                "field": "dest_name"
              }
            }
          },
          {
            "proto": {
              "terms": {
                "field": "proto"
              }
            }
          },
          {
            "dest_ip": {
              "terms": {
                "field": "dest_ip"
              }
            }
          } ,
          {
            "source_ip": {
              "terms": {
                "field": "source_ip"
              }
            }
          },
          {
            "source_port": {
              "terms": {
                "field": "source_port"
              }
            }
          },
          {
            "dest_port": {
              "terms": {
                "field": "dest_port"
              }
            }
          },
          {
            "reporter": {
              "terms": {
                "field": "reporter"
              }
            }
          },
          {
            "action": {
              "terms": {
                "field": "action"
              }
            }
          }
        ]
      },
      "aggs": {
	"policies": {
          "nested": { "path": "policies" },
          "aggs": {
            "by_tiered_policy": {
              "terms": {
              "field": "policies.all_policies"
              }
            }
          }
        },
        "source_labels": {
          "nested": { "path": "source_labels" },
          "aggs": {
            "by_kvpair": {
              "terms": {
              "field": "source_labels.labels"
              }
            }
          }
        },
        "dest_labels": {
          "nested": { "path": "dest_labels" },
          "aggs": {
            "by_kvpair": {
              "terms": {
              "field": "dest_labels.labels"
              }
            }
          }
        },
        "sum_num_flows_started": {
          "sum": {
            "field": "num_flows_started"
          }
        },
        "sum_num_flows_completed": {
          "sum": {
            "field": "num_flows_completed"
          }
        },
        "sum_packets_in": {
          "sum": {
            "field": "packets_in"
          }
        },
        "sum_bytes_in": {
          "sum": {
            "field": "bytes_in"
          }
        },
        "sum_packets_out": {
          "sum": {
            "field": "packets_out"
          }
        },
        "sum_bytes_out": {
          "sum": {
            "field": "bytes_out"
          }
        }
      }
    }
  }
}'
