#!/bin/sh

docker stop es; sleep 5; docker run --rm -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name es docker.elastic.co/elasticsearch/elasticsearch:6.3.2

docker stop kibana; sleep 5; docker run --rm -d -p 5601:5601 --link es -e "ELASTICSEARCH_URL=http://es:9200" --name kibana docker.elastic.co/kibana/kibana:6.3.2
