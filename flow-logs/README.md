# Flow Logs Generator & Elastic/Kibana Setup

- Clone this repo to say `~Code/calico-perf` or download the scripts in this directory to say `~/Code/calico-perf/flow-logs`.
- Then
```
cd ~/Code/calico-perf/flow-logs
```
- Start Elastic Search and Kibana

```
sh run-es.sh
```

- Wait for Elasticsearch and Kibana to start.

- Skip the above 2 steps if you already have Elastic search and Kibana running. Instead to delete old flow logs data run the following command:
```
curl -X DELETE localhost:9200/cnx_flows
```

- At this point, you either have a fresh ES + Kibana cluster or deleted any existing data for the "cnx_flows" mapping.

- Create a mapping:
```
sh flows_schema.sh
```

- It should return `{"acknowledged":true,"shards_acknowledged":true,"index":"cnx_flows"}`
- Also verify by running `curl -X GET localhost:9200/cnx_flows/_mapping/_doc`


- finally create some dummy data. It takes 2 steps.
1. run this command (update the number 20 to 500 or 1000 to generate more flows):

```
python3 gen_flogs.py cnx_flows 20 flows.txt
```

2. The above command prints out a `curl` command. Copy paste that and run this command.


- Try out the query.
```
sh query.sh
```

NOTE:
- If `source_ip` or `dest_ip` returns 0.0.0.0, this means that this is an null field and this value must be ignored.
- If `source_port` or `dest_port` returns 0, this means that this is an null field and this value must be ignored.

- The above instructions also deploy kibana which you can get to by going to http://localhost:5601/

To use Kibana:
- Click on "discover" (the "compass" icon?) in the sidebar.
- Select cnx_flows as the index pattern.
- Click "Next step".
- Select "I don't want to use the Time Filter" (for now).
- Click "Create Index pattern"
- Click on "discover" again to explore the data.
