#!/usr/local/bin/python3
import json
import random
import sys
import time
import ipaddress

pods = ["nginx", "haproxy", "kafka", "django", "cassandra", "memcached", "node"]
cidrs = ["10.0.0.0/8", "192.168.0.0/16", "172.16.0.0/12"]
namespaces = ["-", "dev", "test", "prod"]
types = ["network", "wep"]
actions = ["allow", "deny"]
directions = ["in", "out"]
reporters = ["dest", "source"]
ippool = list(ipaddress.ip_network("192.168.0.0/16").hosts())

def rand_name(type_):
    if type_ == "network":
        return random.choice(cidrs)
    return random.choice(pods) + "-" + str(random.randint(1,9))

def get_policies(ns, action):
    maxTiers = random.randint(1,3)
    tierStep = random.randint(1,2)

    def policy_name(tn, sfx):
        if ns != "-":
            return "{}/{}.policy-{}".format(ns, tn, sfx)
        return "{}.policy-{}".format(tn, sfx)

    policies = [dict(tier="tier-"+str(x),
                     policy=policy_name("tier-"+str(x),
                                       str(random.randint(1,10))),
                     rule_index=str(random.randint(1,10)),
                     action="pass") for x in range(0, maxTiers, tierStep)]
    policies.append(dict(tier="tier-"+str(maxTiers),
                         policy=policy_name("tier-"+str(maxTiers),
                                           action),
                      rule_index=str(random.randint(1,10)),
                      action=action))
    traces = []
    i = 0
    for policy in policies:
        traces.append("{}|{}|{}|{}".format(i, policy['tier'], policy['policy'],
                                           policy['action']))
        i += 1
    return dict(all_policies=traces)

def get_labels(name, ns, type_, include_type=False):
    if type_ == "wep":
        d = [dict(key="app", value=name.split("-")[0]),
             dict(key="namespace", value=ns)]
        if include_type:
            d.append(dict(key="type", value=type_))
        return d
    else:
        return {}

def flatten_labels(labels):
    if not labels:
        return {}
    lbls = []
    for l in labels:
        lbls.append("{}={}".format(l['key'], l['value']))
    return dict(labels=lbls)

# Emit a series of flow logs
def emit_logs(start_time, out_file):
    stype = random.choice(types)
    if stype == "network":
        dtype = "wep"
    else:
        dtype = random.choice(types)
    sname = rand_name(stype)
    dname = rand_name(dtype)
    sns = random.choice(namespaces)
    dns = random.choice(namespaces)

    action = random.choice(actions)
    policies = get_policies(dns, action)

    slabels = flatten_labels(get_labels(sname, sns, stype))
    dlabels = flatten_labels(get_labels(dname, dns, dtype))

    log = {
        "policies": policies,
        "source_labels": slabels,
        "dest_labels": dlabels,
        "source_type": stype,
        "source_name": sname,
        "source_namespace": sns,
        "dest_type": dtype,
        "dest_name": dname,
        "dest_namespace": dns,
        "reporter": random.choice(reporters),
        "action": action,
        #"node": "node-" + str(random.randint(1,10)),
        # The following fields are updated per iteration
        "num_flows": 0,
        "num_flows_started": 0, # I'm assuming these fields are 'in the last period'
        "num_flows_completed": 0,# ^^
        "start_time": 0,
        "end_time": 0,
        "packets_in": 0,
        "packets_out": 0,
        "bytes_in": 0,
        "bytes_out": 0,
        "proto": "6",
        "source_ip": str(random.choice(ippool)),
        "dest_ip": str(random.choice(ippool)),
        "source_port": "-",
        "dest_port": str(random.choice([22, 80, 443, 8000, 8080])),
    }

    if dtype == "network":
        log["reporter"] = "source"

    flog_repeats = random.randint(1, 60)
    flow_multi = random.randint(1, 5) * random.randint(1, 5)
    pkt_multi = random.randint(2, 10) * random.randint(2, 10)
    flog_start = start_time
    for ii in range(flog_repeats):
        # Log period
        log["start_time"] = flog_start
        period_length = 50 + random.randint(1, 20)
        flog_start += period_length
        log["end_time"] = flog_start

        # Flows calculation split around packets calculation so we can use old + new flows
        log["num_flows_started"] = random.randint(0, flog_repeats - ii) * random.randint(1, flow_multi)
        log["num_flows"] += log["num_flows_started"]

        # The following are calculated fields; probably computed in logstash / fluentd
        log["packets_in_rate"] = random.randint(1, pkt_multi) * log["num_flows"]
        log["packets_out_rate"] = random.randint(1, pkt_multi) * log["num_flows"]
        log["bytes_in_rate"] = random.randint(100, 1500) * log["packets_in_rate"]
        log["bytes_out_rate"] = random.randint(100, 1500) * log["packets_out_rate"]
        log["packets_in"] += log["packets_in_rate"] * period_length
        log["packets_out"] += log["packets_out_rate"] * period_length
        log["bytes_in"] += log["bytes_in_rate"] * period_length
        log["bytes_out"] += log["bytes_out_rate"] * period_length

        # Resume flows calculation
        log["num_flows_completed"] = int((random.randint(0, ii + 2) * log["num_flows"]) / flog_repeats)
        if ii == flog_repeats - 1:
            log["num_flows_completed"] = log["num_flows"]
        log["num_flows"] -= log["num_flows_completed"]

        if log["action"] == "allow":
            log["source_ip"] = "-"
            log["dest_ip"] = "-"

        keys_to_remove = ["packets_in_rate", "packets_out_rate", "bytes_in_rate", "bytes_out_rate"]
        #for k, v in log.items():
        #    if v == "-":
        #        print("Removing key " + k)
        #        keys_to_remove.append(k)

        d = dict(log)
        for p in keys_to_remove:
            d.pop(p)
        for k, v in log.items():
            if v == "-":
                d[k] = None

        # Occasionally mess up the tiers.
        if random.randint(1,10) == 7:
            d["policies"] = get_policies(dns, action)
            if log["source_labels"]:
                d["source_labels"] = flatten_labels(get_labels(sname,
                                                               sns,
                                                               stype,
                                                               include_type=True))
            if log["dest_labels"]:
                d["dest_labels"] = flatten_labels(get_labels(dname,
                                                             dns,
                                                             dtype,
                                                             include_type=True))

        # Output the flog, in bulk format
        command = {"index": {}}
        print(json.dumps(command), file=out_file)
        #print(json.dumps(log), file=out_file)
        print(json.dumps(d), file=out_file)

        # Make sure we create the scenarios:
        if (log["action"] == "allow" and
                log["source_type"] != "network" and
                log["dest_type"] != "network"):
            # Scenario 3.
            reverse_d = dict(d)
            if log["reporter"] == "source":
                reverse_d["reporter"] = "dest"
            else:
                reverse_d["reporter"] = "source"
            reverse_d["policies"] = get_policies(sns, reverse_d["action"])

            command = {"index": {}}
            print(json.dumps(command), file=out_file)
            #print(json.dumps(log), file=out_file)
            print(json.dumps(reverse_d), file=out_file)
            if random.randint(1,20) == 4:
                # Occasionally generate a deny flow. Scenario 4.
                deny_d = dict(d)
                deny_d["action"] = "deny"
                maxTiers = random.randint(1,9)
                tierStep = random.randint(1,3)
                deny_d["policies"] = get_policies(dns, "deny")

                command = {"index": {}}
                print(json.dumps(command), file=out_file)
                #print(json.dumps(log), file=out_file)
                print(json.dumps(deny_d), file=out_file)
        elif log["action"] == "deny" and log["reporter"] == "dest":
                # Scenario 2
                reverse_d = dict(d)
                reverse_d["reporter"] = "source"
                reverse_d["action"] = "allow"
                reverse_d["source_ip"] = None
                reverse_d["dest_ip"] = None

                reverse_d["policies"] = get_policies(sns, "allow")

                command = {"index": {}}
                print(json.dumps(command), file=out_file)
                #print(json.dumps(log), file=out_file)
                print(json.dumps(reverse_d), file=out_file)

if len(sys.argv) != 4:
    print("python gen_flogs.py <index name> <number of flows> <file>")
    sys.exit(1)
index = sys.argv[1]
num_flogs = int(sys.argv[2])
out_file = open(sys.argv[3], 'w')

# Create flogs covering a period of one hour approximately
start_time = int(time.time()) - 3600

# I'm using an export interval of one minute here
for ii in range(num_flogs):
    flog_start = start_time + random.randint(1, 3600)
    emit_logs(flog_start, out_file)


print("Emitted flow logs to file.  Load them into Elasticsearch with:")
print("curl -X POST localhost:9200/" + index + "/_doc/_bulk -H 'Content-Type: application/json' --data-binary @" + sys.argv[3])
