package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/projectcalico/libcalico-go/lib/apis/v3"
	"github.com/projectcalico/libcalico-go/lib/clientv3"
	"github.com/projectcalico/libcalico-go/lib/names"
	"github.com/projectcalico/libcalico-go/lib/options"
	"github.com/sirupsen/logrus"
)

var (
	scrapePeriod     = flag.Int("scrape", 10, "The scrape-period in seconds. Only valid for prometheus-config command.")
	doNotConsolidate = flag.Bool("do-not-consolidate", false, "Whether to not add recording rules which consolide data "+
		"across instances. Only valid for prometheus-config command.")
	firstAddr  = flag.String("listen-addr", "localhost:12000", "The address to listen on for HTTP requests in form <addr>:<port>")
	tiers      = flag.Int("tiers", 10, "The number of tiers.")
	policies   = flag.Int("policies", 10, "The number of policies in each tier.")
	namespaces = flag.Int("namespaces", 10, "The number of namespaces.")
	rules      = flag.Int("rules", 50, "The number of rules in each policy.")
	nodes      = flag.Int("nodes", 1000, "The number of nodes. Each node uses a port number incrementing from the "+
		"first port number specified using --listen-addr.")
	perNode = flag.Int("percent-per-node", 10, "The percentage of rules that apply per node. If too small this will be "+
		"adjusted to ensure all rules are covered in the stats.")
	delMetrics = flag.Int("metrics-to-delete", 10, "The number of metrics to delete each iteration of the "+
		"generation loop. These metrics get added back immediately, but it works to generate additional churn. Only "+
		"valid for the serve command.")
	delPause = flag.Float64("delete-pause", 15.0, "The number of seconds to pause after deleting a set of metrics "+
		"(useful to ensure prometheus scraping always spots the deleted metrics). Only valid for the serve command.")
	weps = flag.Int("weps", 50, "The number of workload endpoints per node. Only valid for configure-calico command.")
	heps = flag.Int("heps", 2, "The number of host endpoints per node. Only valid for configure-calico command.")
	knp  = flag.Bool("knp", false, "Mimic kubernetes policy in the default tier.")

	trafficDirections = []string{"inbound", "outbound"}
	ruleDirections    = []string{"ingress", "egress"}
	actions           = []string{"allow", "deny", "pass"}
	oscPeriod         = 30 * time.Minute
	oscAmp            = 20000.0
)

const (
	numServersPerRack = 10
	globalNS          = "__GLOBAL__"
	noMatchPolicy     = "__NO_MATCH__"
	profileTier       = "__PROFILE__"
)

type key struct {
	tier             string
	policy           string
	namespace        string
	rules            string
	action           string
	trafficDirection string
	ruleDirection    string
	instance         string
}

var labels = []string{"tier", "policy", "namespace", "rule_index", "action", "traffic_direction", "rule_direction", "instance"}
var connectionLabels = []string{"tier", "policy", "namespace", "rule_index", "traffic_direction", "rule_direction", "instance"}

func (k key) labelValues() []string {
	return []string{k.tier, k.policy, k.namespace, k.rules, k.action, k.trafficDirection, k.ruleDirection, k.instance}
}

func (k key) connectionLabelValues() []string {
	return []string{k.tier, k.policy, k.namespace, k.rules, k.trafficDirection, k.ruleDirection, k.instance}
}

func main() {
	logrus.SetLevel(logrus.WarnLevel)
	flag.Usage = func() {
		fmt.Printf("Usage:\n    %s serve\n    %s configure-calico\n    %s prometheus-config\n\n", os.Args[0], os.Args[0], os.Args[0])
		flag.PrintDefaults()
	}

	// Trace out the full command.
	cstring := ""
	for _, arg := range os.Args {
		if strings.Contains(arg, " ") {
			cstring += " \"" + arg + "\""
		} else {
			cstring += " " + arg
		}
	}
	fmt.Printf("Command: %v\n", cstring)

	// Extract the sub command, and parse the flags.
	cmd := "serve"
	if len(os.Args) > 1 && os.Args[1][0] != '-' {
		cmd = os.Args[1]
		flag.CommandLine.Parse(os.Args[2:])
	} else {
		flag.CommandLine.Parse(os.Args[1:])
	}

	// Output config summary.
	fmt.Printf("Sub command: %v\n", cmd)
	fmt.Printf("Num nodes: %v\n", *nodes)
	fmt.Printf("Num namespaces: %v\n", *namespaces)
	fmt.Printf("Num WEPs per node: %v\n", *weps)
	fmt.Printf("Num HEPs per node: %v\n", *heps)
	fmt.Printf("Num tiers: %v\n", *tiers)
	fmt.Printf("Num policies per tier: %v\n", *policies)
	fmt.Printf("Num rules per policy: %v\n", *rules)

	// If the per-node value is too small, adjust it - we want to ensure all rules are covered by the nodes.  This is
	// ust a failsafe in case the user selects a very small number of nodes.
	if *perNode < (100 / *nodes) {
		*perNode = 100 / *nodes
	}

	// Run the requested sub command.
	switch cmd {
	case "serve":
		serve()
	case "configure-calico":
		configureCalico()
	case "prometheus-config":
		prometheusConfig()
	default:
		flag.Usage()
		os.Exit(1)
	}
}

// serve the Prometheus stats based on the CLI args.
func serve() {
	var metrics []key
	instances := make([]*prometheus.Registry, *nodes)
	policyRuleBytes := make([]*prometheus.CounterVec, *nodes)
	policyRulePackets := make([]*prometheus.CounterVec, *nodes)
	policyRuleConnections := make([]*prometheus.GaugeVec, *nodes)

	// Create a new prometheus registry for each emulated node.
	for n := 0; n < *nodes; n++ {
		policyRuleBytes[n] = prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cnx_policy_rule_bytes",
				Help: "Denied Bytes.",
			},
			labels,
		)
		policyRulePackets[n] = prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cnx_policy_rule_packets",
				Help: "Denied Packets.",
			},
			labels,
		)
		policyRuleConnections[n] = prometheus.NewGaugeVec(
			prometheus.GaugeOpts{
				Name: "cnx_policy_rule_connections",
				Help: "Total number of connections allowed by CNX policies.",
			},
			connectionLabels,
		)
		instances[n] = prometheus.NewRegistry()
		instances[n].MustRegister(policyRuleBytes[n])
		instances[n].MustRegister(policyRulePackets[n])
		instances[n].MustRegister(policyRuleConnections[n])
	}

	// Split the listen address to determine the first port index.  Each node is handled
	// by consecutive port numbers.
	addrParts := strings.Split(*firstAddr, ":")
	if len(addrParts) != 2 {
		panic("Address format is incorrect")
	}
	addr := addrParts[0]
	port, err := strconv.ParseInt(addrParts[1], 10, 16)
	if err != nil {
		panic("Address format is incorrect")
	}

	// Generate all of the required time-series metrics.  Each node will use a certain
	// subset of these metrics. The action is tied to the rule, so let's just iterate
	// through the actions for each rule.
	for t := 0; t < *tiers; t++ {
		ingressImplicitAssigned := false
		egressImplicyAssigned := false
		for p := 0; p < *policies; p++ {
		processRules:
			// Start from `-1` for implicit drop.
			for r := -1; r < *rules; r++ {
				namespace := namespaceForPolicy(t, p)
				for td := 0; td < len(trafficDirections); td++ {
					for rd := 0; rd < len(ruleDirections); rd++ {
						var action, pn string
						ruleDirection := ruleDirections[rd]
						if r == -1 {
							if ingressImplicitAssigned && egressImplicyAssigned {
								continue processRules
							}
							pn = policyNameShortened(t, *policies-1)
							action = "deny"
							if ruleDirection == "ingress" {
								ingressImplicitAssigned = true
							} else if ruleDirection == "egress" {
								egressImplicyAssigned = true
							}
						} else {
							pn = policyNameShortened(t, p)
							action = actions[r%len(actions)]
						}
						k := key{
							tier:             tierName(t),
							policy:           pn,
							namespace:        namespace,
							rules:            fmt.Sprintf("%d", r),
							action:           action,
							trafficDirection: trafficDirections[td],
							ruleDirection:    ruleDirection,
						}
						metrics = append(metrics, k)
					}
				}
			}
		}
		// Generate end of tier implicit drop stats
		for rd := 0; rd < len(ruleDirections); rd++ {
			namespace := globalNS
			k := key{
				tier:             tierName(t),
				policy:           noMatchPolicy,
				namespace:        namespace,
				rules:            "rule-0",
				action:           "deny",
				trafficDirection: trafficDirections[rd],
				ruleDirection:    ruleDirections[rd],
			}
			metrics = append(metrics, k)
		}
	}
	for r := 0; r < *rules; r++ {
		namespace := namespaceForPolicy(0, r)
		for td := 0; td < len(trafficDirections); td++ {
			for rd := 0; rd < len(ruleDirections); rd++ {
				var action, pn string
				ruleDirection := ruleDirections[rd]
				action = actions[r%len(actions)]
				pn = namespaceProfileName(namespace)
				k := key{
					tier:             profileTier,
					policy:           pn,
					namespace:        namespace,
					rules:            fmt.Sprintf("%d", r),
					action:           action,
					trafficDirection: trafficDirections[td],
					ruleDirection:    ruleDirection,
				}
				metrics = append(metrics, k)
			}
		}
	}

	// Generate end of tier implicit drop stats
	for rd := 0; rd < len(ruleDirections); rd++ {
		namespace := globalNS
		k := key{
			tier:             profileTier,
			policy:           noMatchPolicy,
			namespace:        namespace,
			rules:            "0",
			action:           "deny",
			trafficDirection: trafficDirections[rd],
			ruleDirection:    ruleDirections[rd],
		}
		metrics = append(metrics, k)
	}
	fmt.Println("Generated metrics are:")
	for _, k := range metrics {
		fmt.Printf("%+v\n", k)
	}

	// Start serving up data for those metrics for each node.  We assume each node produces
	// data for certain percentage of the total set of metrics.
	for n := 0; n < *nodes; n++ {
		go func(nodeNum int) {
			fmt.Printf("Started load generation for node-%d\n", nodeNum)
			defer fmt.Printf("Stopped load generation for node-%d\n", nodeNum)

			// Depending on the node, we use a sub-set of the total number of metrics.  Calculate
			// the start and end metric idx (which may be > len(metric), but should wrap using modulo)
			startMetric := len(metrics) * nodeNum / *nodes
			endMetric := startMetric + (*perNode * len(metrics) / 100)

			startTime := time.Now()
			lastTime := startTime

			// Loop forever generating data.
			for {
				// Generate some stats for 10s.  Generate some nice data.
				fmt.Printf("Generating metrics for node-%d\n", nodeNum)
				thisTime := time.Now()
				for i := 0; i < 10; i++ {
					for idx := startMetric; idx < endMetric; idx++ {
						metric := metrics[idx%len(metrics)]
						metric.instance = nodeName(nodeNum)
						rateValue := rateValue(startTime, thisTime, nodeNum)
						deltaValue := rateValue * (thisTime.Sub(lastTime).Seconds())
						policyRuleBytes[nodeNum].WithLabelValues(metric.labelValues()...).Add(deltaValue + (5.0 * float64(idx)))
						policyRulePackets[nodeNum].WithLabelValues(metric.labelValues()...).Add(deltaValue + (3.0 * float64(idx)))
						policyRuleConnections[nodeNum].WithLabelValues(metric.connectionLabelValues()...).Add(math.Floor(deltaValue) + float64(idx))
					}
					time.Sleep(time.Second)
				}
				lastTime = thisTime

				// Delete some metrics - just delete a contiguous block starting from a random
				// way through the nodes specific set of metrics.
				if *delMetrics > 0 {
					fmt.Printf("Deleting metrics for node-%d\n", nodeNum)
					idx := int(float64(startMetric) + (float64(endMetric-startMetric) * rand.Float64()))
					for i := 0; i < *delMetrics && idx < endMetric; i++ {
						metric := metrics[idx%len(metrics)]
						metric.instance = nodeName(nodeNum)
						policyRuleBytes[nodeNum].DeleteLabelValues(metric.labelValues()...)
						policyRulePackets[nodeNum].DeleteLabelValues(metric.labelValues()...)
						policyRuleConnections[nodeNum].DeleteLabelValues(metric.connectionLabelValues()...)
					}
					time.Sleep(time.Duration(*delPause * float64(time.Second)))
				}
			}
		}(n)

		// Start an http server for this node on the required port using the node-specific
		// prometheus registry.
		go func(nodeNum int) {
			fmt.Printf("Start serving prometheus metrics for node-%d\n", nodeNum)
			defer fmt.Printf("Stopped serving prometheus metrics for node-%d\n", nodeNum)
			nodeAddr := fmt.Sprintf("%s:%d", addr, int(port)+nodeNum)
			mux := http.NewServeMux()
			mux.Handle("/metrics", promhttp.HandlerFor(instances[nodeNum], promhttp.HandlerOpts{}))
			log.Fatal(http.ListenAndServe(nodeAddr, mux))
		}(n)
	}

	// Block forever.
	select {}
}

func rateValue(start time.Time, thisTime time.Time, nodeNum int) float64 {
	theta := 2.0 * math.Pi * float64(nodeNum) / float64(*nodes)
	omega := 2.0 * math.Pi / float64(oscPeriod)
	t := float64(thisTime.Sub(start))
	return oscAmp * 0.5 * (1 + math.Sin(theta+(omega*t)))
}

// configureCalico will configure resources that match the prometheus stats.
func configureCalico() {
	fmt.Println("Generating config")
	client, err := clientv3.NewFromEnv()
	if err != nil {
		fmt.Println("Failed to access datastore: ", err)
		os.Exit(1)
	}

	// Write node config
	fmt.Println("Generating Node config")
	for i := 0; i < *nodes; i++ {
		r := nodeConfig(i)
		_, err := client.Nodes().Create(context.Background(), r, options.SetOptions{})
		if err != nil {
			fmt.Println("Failed to create node: ", r.Name, err)
		}
	}
	// Write host endpoint config
	fmt.Println("Generating HEP config")
	for i := 0; i < *nodes; i++ {
		for j := 0; j < *heps; j++ {
			r := hepConfig(i, j)
			_, err := client.HostEndpoints().Create(context.Background(), r, options.SetOptions{})
			if err != nil {
				fmt.Println("Failed to create host endpoint: ", r.Name, err)
			}
		}
	}
	// Write workload endpoint config
	fmt.Println("Generating WEP config")
	for i := 0; i < *nodes; i++ {
		for j := 0; j < *weps; j++ {
			r := wepConfig(i, j)
			_, err := client.WorkloadEndpoints().Create(context.Background(), r, options.SetOptions{})
			if err != nil {
				fmt.Println("Failed to create workload endpoint: ", r.Namespace, r.Name, err)
			}
		}
	}
	// Write tier config
	fmt.Println("Generating Tier config")
	for i := 0; i < *tiers; i++ {
		r := tierConfig(i)
		_, err := client.Tiers().Create(context.Background(), r, options.SetOptions{})
		if err != nil {
			fmt.Println("Failed to create tier: ", r.Name, err)
		}
	}
	// Write namespace config
	fmt.Println("Generating Namespace (profile) config")
	for i := 0; i < *namespaces; i++ {
		r := namespaceConfig(i)
		_, err := client.Profiles().Create(context.Background(), r, options.SetOptions{})
		if err != nil {
			fmt.Println("Failed to create profile: ", r.Name, err)
		}
	}
	// Write policy config
	fmt.Println("Generating Policy config")
	for i := 0; i < *tiers; i++ {
		for j := 0; j < *policies; j++ {
			p := policyConfig(i, j)
			switch r := p.(type) {
			case *v3.NetworkPolicy:
				_, err := client.NetworkPolicies().Create(context.Background(), r, options.SetOptions{})
				if err != nil {
					fmt.Println("Failed to create network policy: ", r.Namespace, r.Name, err)
				}
			case *v3.GlobalNetworkPolicy:
				_, err := client.GlobalNetworkPolicies().Create(context.Background(), r, options.SetOptions{})
				if err != nil {
					fmt.Println("Failed to create global network policy: ", r.Name, err)
				}
			}
		}
	}
}

func nodeConfig(n int) *v3.Node {
	node := v3.NewNode()
	node.Name = nodeName(n)
	node.Labels = map[string]string{
		"location": fmt.Sprintf("rack-%d", rack(n)),
		"server":   fmt.Sprintf("%d", server(n)),
	}
	if n >= 2 {
		node.Spec.BGP = &v3.NodeBGPSpec{
			IPv4Address: ipv4(n),
			IPv6Address: ipv6(n),
		}
	}
	return node
}

func nodeName(n int) string {
	return fmt.Sprintf("node-%d", n)
}

func ipv4(i int) string {
	v := 0xC00A0000 + i
	v3 := byte(v & 0xFF)
	v2 := byte((v >> 8) & 0xFF)
	v1 := byte((v >> 16) & 0xFF)
	v0 := byte((v >> 24) & 0xFF)
	return net.IP{v0, v1, v2, v3}.String()
}

func ipv6(i int) string {
	v := 0xC00A0000 + i
	v3 := byte(v & 0xFF)
	v2 := byte((v >> 8) & 0xFF)
	v1 := byte((v >> 16) & 0xFF)
	v0 := byte((v >> 24) & 0xFF)
	ip6 := net.IP{v0, v1, v2, v3}.To16()
	ip6[0] = 0xaa
	ip6[1] = 0xaa
	ip6[2] = 0xaa
	ip6[3] = 0xaa
	return ip6.String()
}

func rack(n int) int {
	return n / numServersPerRack
}

func server(n int) int {
	return n % numServersPerRack
}

func hepConfig(n, h int) *v3.HostEndpoint {
	idx := (n * *heps) + h
	hep := v3.NewHostEndpoint()
	hep.Name = fmt.Sprintf("%s-eth%d", nodeName(n), h)
	// We'll make 2% of the HEPs unlabelled and 2% labelled but unselectable. We don't have
	// any policy matching on unknown location or server, although we do have rules that
	// match on those.
	if idx%50 == 2 {
		// Labelled but unselectable (2%)
		hep.Labels = map[string]string{
			"location": "unknown",
			"server":   "unknown",
			"host":     "yes",
		}
	} else if idx%50 != 4 {
		// Selectable (96%)
		hep.Labels = map[string]string{
			"location": fmt.Sprintf("rack-%d", rack(n)),
			"server":   fmt.Sprintf("%d", server(n)),
			"host":     "yes",
		}
	}
	hep.Spec.Node = nodeName(n)
	hep.Spec.InterfaceName = fmt.Sprintf("eth%d", h)
	hep.Spec.ExpectedIPs = []string{ipv4(idx), ipv6(idx)}
	return hep
}

func wepConfig(n, w int) *v3.WorkloadEndpoint {
	idx := (n * *weps) + w
	wep := v3.NewWorkloadEndpoint()
	wid := wepIDs(n, w)
	wep.Name, _ = wid.CalculateWorkloadEndpointName(false)
	wep.Namespace = nextNamespace()

	// We'll make 2% of the WEPs unlabelled and 2% labelled but unselectable. We don't have
	// any policy matching on unknown location or server, although we do have rules that
	// match on those.
	if idx%50 == 2 {
		// Labelled but unselectable (2%)
		wep.Labels = map[string]string{
			"location": "unknown",
			"server":   "unknown",
			"host":     "no",
		}
	} else if idx%50 != 4 {
		// Selectable (96%)
		wep.Labels = map[string]string{
			"location": fmt.Sprintf("rack-%d", rack(n)),
			"server":   fmt.Sprintf("%d", server(n)),
			"host":     "no",
		}
	}
	wep.Spec.Node = wid.Node
	wep.Spec.Orchestrator = wid.Orchestrator
	wep.Spec.Pod = wid.Pod
	wep.Spec.ContainerID = wid.ContainerID
	wep.Spec.Endpoint = wid.Endpoint
	wep.Spec.Workload = wid.Workload
	wep.Spec.InterfaceName = fmt.Sprintf("cali0123%d%d", w, n)
	wep.Spec.MAC = "ca:fe:1b:ad:be:ef"
	wep.Spec.IPNetworks = []string{ipv4Net(idx), ipv6Net(idx)}
	wep.Spec.Profiles = []string{namespaceProfileName(wep.Namespace)}
	return wep
}

func ipv4Net(i int) string {
	v := 0x0A0A0000 + i
	v3 := byte(v & 0xFF)
	v2 := byte((v >> 8) & 0xFF)
	v1 := byte((v >> 16) & 0xFF)
	v0 := byte((v >> 24) & 0xFF)
	return net.IP{v0, v1, v2, v3}.String() + "/32"
}

func ipv6Net(i int) string {
	v := 0x0A0A0000 + i
	v3 := byte(v & 0xFF)
	v2 := byte((v >> 8) & 0xFF)
	v1 := byte((v >> 16) & 0xFF)
	v0 := byte((v >> 24) & 0xFF)
	ip6 := net.IP{v0, v1, v2, v3}.To16()
	ip6[0] = 0xaa
	ip6[1] = 0xaa
	ip6[2] = 0xaa
	ip6[3] = 0xaa
	return ip6.String() + "/128"
}

func wepIDs(n, w int) names.WorkloadEndpointIdentifiers {
	return names.WorkloadEndpointIdentifiers{
		Node:         nodeName(n),
		Orchestrator: "k8s",
		Endpoint:     "eth0",
		Workload:     "",
		Pod:          fmt.Sprintf("pod-%d", w+(n**weps)),
		ContainerID:  fmt.Sprintf("container-%d", w+(n**weps)),
	}
}

func tierConfig(t int) *v3.Tier {
	tier := v3.NewTier()
	tier.Name = tierName(t)
	if t != 0 {
		v := float64(t)
		tier.Spec.Order = &v
	}
	return tier
}

func tierName(t int) string {
	if t == 0 {
		return "default"
	}
	return fmt.Sprintf("tier-%d", t)
}

func namespaceConfig(n int) *v3.Profile {
	profile := v3.NewProfile()
	profile.Name = namespaceProfileName(namespaceName(n))
	profile.Labels = map[string]string{
		"type": fmt.Sprintf("type-%d", n),
	}
	return profile
}

func namespaceProfileName(ns string) string {
	return "kns." + ns
}

func namespaceName(n int) string {
	return fmt.Sprintf(`namespace-%d`, n)
}

var ns = -1

func nextNamespace() string {
	ns = (ns + 1) % *namespaces
	return namespaceName(ns)
}

func policyConfig(t, p int) interface{} {
	if t == 0 {
		// For this emulated config, all tier 0 (default) config is k8s network policy.
		return npConfig(t, p)
	}

	// For other tiers, flip between NP and GNPs.
	idx := ((t * *policies) + p)
	if idx%2 == 0 {
		return gnpConfig(t, p)
	}
	return npConfig(t, p)
}

func namespaceForPolicy(t, p int) string {
	// This matches the behavior in policyConfig (i.e. for GNP we always return __GLOBAL__).

	idx := ((t * *policies) + p)
	if t != 0 && idx%2 == 0 {
		return globalNS
	}
	return namespaceName(idx % *namespaces)
}

func gnpConfig(t, p int) *v3.GlobalNetworkPolicy {
	o := float64(p)
	policy := v3.NewGlobalNetworkPolicy()
	policy.Name = policyName(t, p)
	policy.Spec.Order = &o
	policy.Spec.Tier = tierName(t)
	policy.Spec.Ingress = ingressRules(t, p)
	policy.Spec.Egress = egressRules(t, p)
	return policy
}

func npConfig(t, p int) *v3.NetworkPolicy {
	idx := (t * *policies) + p
	o := float64(p)
	policy := v3.NewNetworkPolicy()
	policy.Name = policyName(t, p)
	policy.Namespace = namespaceForPolicy(t, p)
	policy.Spec.Selector = fmt.Sprintf("server == '%d'", idx%numServersPerRack)
	policy.Spec.Order = &o
	policy.Spec.Types = []v3.PolicyType{v3.PolicyTypeEgress, v3.PolicyTypeIngress}
	policy.Spec.Tier = tierName(t)
	policy.Spec.Ingress = ingressRules(t, p)
	policy.Spec.Egress = egressRules(t, p)
	return policy
}

func ingressRules(t, p int) []v3.Rule {
	idx := (t * *policies) + p
	rs := make([]v3.Rule, *rules)
	for r := 0; r < *rules-2; r++ {
		rs[r] = v3.Rule{
			Action: v3.Allow,
			Source: v3.EntityRule{
				Selector:    fmt.Sprintf("server == '%d' && host == 'yes'", (idx+1)%numServersPerRack),
				NotSelector: fmt.Sprintf("server == '%d' && host == 'yes'", (idx+2)%numServersPerRack),
			},
			Destination: v3.EntityRule{
				Selector:    fmt.Sprintf("server == '%d' && host == 'yes'", (idx+3)%numServersPerRack),
				NotSelector: fmt.Sprintf("server == '%d' && host == 'yes'", (idx+4)%numServersPerRack),
			},
		}
	}
	rs[*rules-2] = v3.Rule{
		Action: v3.Deny,
		Source: v3.EntityRule{
			Selector: "server == 'unknown' && host == 'yes'",
		},
		Destination: v3.EntityRule{
			Selector: "server == 'unknown' && host == 'yes'",
		},
	}
	rs[*rules-1] = v3.Rule{
		Action: v3.Pass,
	}
	return rs
}

func egressRules(t, p int) []v3.Rule {
	idx := (t * *policies) + p
	rs := make([]v3.Rule, *rules)
	for r := 0; r < *rules-2; r++ {
		rs[r] = v3.Rule{
			Action: v3.Allow,
			Source: v3.EntityRule{
				Selector:    fmt.Sprintf("server == '%d' && host == 'no'", (idx+1)%numServersPerRack),
				NotSelector: fmt.Sprintf("server == '%d' && host == 'no'", (idx+2)%numServersPerRack),
			},
			Destination: v3.EntityRule{
				Selector:    fmt.Sprintf("server == '%d' && host == 'no'", (idx+3)%numServersPerRack),
				NotSelector: fmt.Sprintf("server == '%d' && host == 'no'", (idx+4)%numServersPerRack),
			},
		}
	}
	rs[*rules-2] = v3.Rule{
		Action: v3.Deny,
		Source: v3.EntityRule{
			Selector: "server == 'unknown' && host == 'no'",
		},
		Destination: v3.EntityRule{
			Selector: "server == 'unknown' && host == 'no'",
		},
	}
	rs[*rules-1] = v3.Rule{
		Action: v3.Pass,
	}
	return rs
}

func policyName(t, p int) string {
	if *knp && t == 0 {
		return policyNameShortened(t, p)
	}
	return tierName(t) + "." + policyNameShortened(t, p)
}

func policyNameShortened(t, p int) string {
	if *knp && t == 0 {
		return fmt.Sprintf(`knp.default.policy-%d`, p)
	}
	return fmt.Sprintf(`policy-%d`, p)
}

// prometheusConfig writes out the prometheus configuration.
func prometheusConfig() {
	// Split the listen address to determine the first port index.  Each node is handled
	// by consecutive port numbers.
	addrParts := strings.Split(*firstAddr, ":")
	if len(addrParts) != 2 {
		panic("Address format is incorrect")
	}
	addr := addrParts[0]
	port, err := strconv.ParseInt(addrParts[1], 10, 16)
	if err != nil {
		panic("Address format is incorrect")
	}

	pCfg := fmt.Sprintf(`
global:
  scrape_interval:     %ds
  scrape_timeout:      %ds   # this should be less than the scrape interval

  # Attach these labels to any time series or alerts when communicating with
  # external systems (federation, remote storage, Alertmanager).
  external_labels:
    monitor: 'codelab-monitor'

`, *scrapePeriod, *scrapePeriod/2)

	if !*doNotConsolidate {
		pCfg += `
rule_files:
  - '/etc/prometheus/rules.yml'
`
	}

	// Add a scrape target for each emulated node.
	pCfg += `
scrape_configs:
  - job_name: 'tigera-metrics'
    static_configs:
      - targets:
`
	for n := 0; n < *nodes; n++ {
		pCfg += fmt.Sprintf("        - \"%s:%d\"\n", addr, int(port)+n)
	}

	if err := ioutil.WriteFile("prometheus-config/prometheus.yml", []byte(pCfg), 0644); err != nil {
		panic(err)
	}

	if !*doNotConsolidate {
		rCfg := fmt.Sprintf(`
groups:
  - name: consolidate
    interval: %ds
    rules:
      - record: cnx_policy_rule_bytes_consolidated
        expr: sum(cnx_policy_rule_bytes) by (tier, policy, namespace, rule_index, action, traffic_direction, rule_direction, instance)
      - record: cnx_policy_rule_packets_consolidated
        expr: sum(cnx_policy_rule_packets) by (tier, policy, namespace, rule_index, action, traffic_direction, rule_direction, instance)
      - record: cnx_policy_rule_connections_consolidated
        expr: sum(cnx_policy_rule_connections) by (tier, policy, namespace, rule_index, traffic_direction, rule_direction, instance)

`, *scrapePeriod)

		if err := ioutil.WriteFile("prometheus-config/rules.yml", []byte(rCfg), 0644); err != nil {
			panic(err)
		}
	}
}
