#!/bin/bash

SOURCE=node
SINK=collector
TIMESTAMP=`date -u +'%s'`

ansible-playbook -i inventory setup.yml -e "source=$SOURCE sink=$SINK timestamp=$TIMESTAMP"
for TEST_NAME in `ls tests`; do
    for WORKLOAD_NAME in `ls workloads`; do
        ansible-playbook -i inventory run_test.yml -e "source=$SOURCE sink=$SINK test_name=$TEST_NAME workload_name=$WORKLOAD_NAME timestamp=$TIMESTAMP"
    done
done
#ansible-playbook -i inventory get_results.yml -e "source=$SOURCE sink=$SINK timestamp=$TIMESTAMP"
