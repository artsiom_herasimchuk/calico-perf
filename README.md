
prometheus-scale
-  Contains load-generator and prometheus config generator for testing prometheus behavior at scale
   with the Tigera specific metrics.
-  See prometheus-scale/README.md for details on operation.